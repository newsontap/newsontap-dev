const NewsAPI = require('newsapi');
var constants = require('./../config/env');
const newsapi = new NewsAPI(process.env['news_api_key']);


exports.fetchNewsSource = function(req, res) {
	//console.log("in");
	newsapi.v2.sources({}).then(response => {
		//console.log("response",response);
		response.sources.forEach( function(element){
			//console.log(element);
			db.collection("tblNewsSource").find({"id": element.id}).toArray(function(err, result) {
				console.log(result);
				if(result.length  == 0){
					element.createdAt = new Date(Date.now()).toLocaleString();
					db.collection("tblNewsSource").insert(element,(err,result)=>{
						console.log("added");
					});		
				}
			});
		});
	});
}

/**
 * NAME: fetchCountryList
 * Date: 23-04-2018
 * Author: Prasad Gone
 * This function is used for fetching all the countries from news source table
 */
exports.fetchCountryList = function(req, res) {
	//fetch Countries
	var response = {};
	db.collection("tblNewsSource").distinct('country',{}, function(err, result){
		if(err != null){
 			response.statusCode = err.code;
 			response.status = 'NOT OK';
 			response.data = {};
 			response.desc = 'Something went wrong, please try again.';
 			response.erros = [err];
 		} else {
 			response.statusCode = 200;
 			response.status = 'OK';
 			if(result.length == 0){
 				response.data = [];
 				response.desc = 'Countries not found.';
 			} else {
 				response.data = result;
 				response.desc = 'Successfully fetched countries.';
 			}
 			response.erros = [];
 		}
 		return res.send(response);
	});
}


/**
 * NAME: fetchLanguageList
 * Date: 23-04-2018
 * Author: Prasad Gone
 * This function is used for fetching all the langauges based on  country url query params
 */
exports.fetchLanguageList = function(req, res) {
	//fetch languages
	const params = req.query["q"].trim();
	var response = {};
	var where = {};
	
	if(params != ''){
		var country = params.split(",").map(item => item.trim());;
		console.log(country.length);
		if(country.length > 0){
			where =  {'country': { $in: country}};
		}
	}
	db.collection("tblNewsSource").distinct('language', where, function(err, result){
		if(err != null){
 			response.statusCode = err.code;
 			response.status = 'NOT OK';
 			response.data = {};
 			response.desc = 'Something went wrong, please try again.';
 			response.erros = [err];
 		} else {
 			response.statusCode = 200;
 			response.status = 'OK';
 			if(result.length == 0){
 				response.data = [];
 				response.desc = 'languages not found.';
 			} else {
 				response.data = result;
 				response.desc = 'Successfully fetched languages.';
 			}
 			response.erros = [];
 		}
 		return res.send(response);
	});
}

/**
 * NAME: fetchCategoryList
 * Date: 23-04-2018
 * Author: Prasad Gone
 * This function is used for fetching all the categories based on language and country url query params
 * TODO: Need to clean up and verify url query params
 */
exports.fetchCategoryList = function(req, res) {
	//fetch languages
	const q = req.query["q"].trim();
	const l = req.query["l"].trim();
	var country = q.split(",");
	var language = l.split(",");
	var response = {};
	db.collection("tblNewsSource").distinct('category', {'country': { $in: country}, 'language': {$in: language}}, function(err, result){
		if(err != null){
 			response.statusCode = err.code;
 			response.status = 'NOT OK';
 			response.data = {};
 			response.desc = 'Something went wrong, please try again.';
 			response.erros = [err];
 		} else {
 			response.statusCode = 200;
 			response.status = 'OK';
 			if(result.length == 0){
 				response.data = [];
 				response.desc = 'categories not found.';
 			} else {
 				response.data = result;
 				response.desc = 'Successfully fetched categories.';
 			}
 			response.erros = [];
 		}
 		return res.send(response);
	});
}