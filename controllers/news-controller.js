const NewsAPI = require('newsapi');
var constants = require('./../config/env');
const newsapi = new NewsAPI(process.env['news_api_key']);

/*
 * AUTHOR: Prasad Gone
 * Date: 16-04-2018
 * This function is used for fetching news data from local DB and send response to user request
 */
 exports.fetchNewsForUsers = function( req, res) {
 	const params = req.body;
 	//console.log(params.language);
 	res.set('Content-Type', 'application/json');
  	var response = {};
  	var where = [
	 	{
	 		$lookup:
	 		{
		 		from: 'tblNewsSource',
		 		localField: 'source.id',
		 		foreignField: 'id',
		 		as: 'newsSource'
	 		}
	 	},
	 	{
	 		$match: {'newsSource.language': params.language}
	 	}
 	];
 	db.collection("tblNews").aggregate(where).toArray(function(err, result){
 		if(err != null){
 			response.statusCode = err.code;
 			response.status = 'NOT OK';
 			response.data = {};
 			response.desc = 'Something went wrong, please try again.';
 			response.erros = [err];
 		} else {
 			response.statusCode = 200;
 			response.status = 'OK';
 			if(result.length == 0){
 				response.data = [];
 				response.desc = 'You read all news.';
 			} else {
 				response.data = result;
 				response.desc = 'Successfully fetched news feed.';
 			}
 			response.erros = [];
 		}
 		return res.send(response);
 	})
 }




/*
*Hemant : Create collection with reference to News-Source colletion.
*/

exports.fetchNewsReferenceToNewsSource = function(req,res)
{

	db.collection("tblNewsSource").find({}, function(err, result) {
		if (err)
		{
			//throw err;
			return res.send("error");
		}
		console.log(result);
		result.forEach(function(item){
			console.log(item);
			newsapi.v2.topHeadlines({sources: item.id, pageSize: 100}).then(response => {
				response.articles.forEach(function(element){
					db.collection("tblNews").find({"source.id": element.source.id, 'title': element.title, 'publishedAt': element.publishedAt}).toArray(function(err, result) {
						if(result.length == 0){
							element.createdAt = new Date(Date.now()).toLocaleString();
							element.updatedAt = new Date(Date.now()).toLocaleString();	
							db.collection("tblNews").insert(element);
						}
					});
				});
			});
		});

	});
}