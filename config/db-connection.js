/**
 * Created by Prasad  on 01-04-2018.
 */
var constants = require('./env');

/*
*Hemant Code: 15-04-2018
*/
const MongoClient = require('mongodb').MongoClient;
// Connection URL
const url = process.env['url'];
// Database Name
const dbName = 'NewsOnTap_Dev';
// Use connect method to connect to the server
const connect = MongoClient.connect(url, function(err, client) {
  console.log("Connected successfully to server");
  global.db = client.db(dbName);
  //console.log(global.db)
});

module.exports = connect;
