var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');
//Hemant
const mongo_connection = require("./config/db-connection");

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');


var app = express();

//var db = require('./config/db-connection');

const newsApiController = require('./controllers/news-controller');
const newsSourceApiController = require('./controllers/news-source-controller');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/users', usersRouter);

//Hemant
app.get('/fetchNewsReferenceToNewsSource', newsApiController.fetchNewsReferenceToNewsSource);
app.get('/api/v1/fetchNewsForUsers', newsApiController.fetchNewsForUsers);


app.get('/fetchNewsSource', newsSourceApiController.fetchNewsSource);
app.get('/api/v1/fetchCountryList', newsSourceApiController.fetchCountryList);
app.get('/api/v1/fetchLanguageList', newsSourceApiController.fetchLanguageList);
app.get('/api/v1/fetchCategoryList', newsSourceApiController.fetchCategoryList);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.
  status(err.status || 500);
  res.render('error');
});

module.exports = app;
